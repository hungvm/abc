Add-WindowsFeature Web-Server
Add-WindowsFeature Web-Asp-Net -IncludeAllSubFeature
Add-WindowsFeature Web-Asp-Net45 -IncludeAllSubFeature
Add-WindowsFeature Web-Mgmt-Compat -IncludeAllSubFeature
Add-WindowsFeature NET-Framework-Features -IncludeAllSubFeature
Add-WindowsFeature NET-Framework-45-ASPNET
Add-WindowsFeature NET-Framework-45-Core
Add-WindowsFeature NET-WCF-HTTP-Activation45
Add-WindowsFeature Web-Common-Http -IncludeAllSubFeature
Add-WindowsFeature Web-Health -IncludeAllSubFeature
Import-Module ServerManager
Add-WindowsFeature Web-Scripting-Tools
Import-Module WebAdministration